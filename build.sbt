import com.typesafe.sbt.packager.archetypes.JavaAppPackaging

lazy val bst = project.in(file(".")).enablePlugins(JavaAppPackaging)

maintainer := "Jakub Nowak"

name := "persistent-bst"

scalaVersion := "2.11.8"

sbtVersion := "0.13.11"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.4",
  "com.storm-enroute" %% "scalameter" % "0.7",
  "org.scalacheck" %% "scalacheck" % "1.12.5" % "test"
)

val scalaMeterFramework = new TestFramework("org.scalameter.ScalaMeterFramework")

testFrameworks in ThisBuild += scalaMeterFramework

testOptions in ThisBuild += Tests.Argument(scalaMeterFramework, "-silent")

parallelExecution in Test := false

fork in run := true