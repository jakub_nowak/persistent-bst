package org.bitbucket.persistentbst.impl

import org.bitbucket.persistentbst._

sealed trait AATree[A, +B] extends TreeLike[A, B] {
  def level: Int

  override def left: AATree[A, B]

  override def right: AATree[A, B]

  override def insert[B1 >: B](k: A, v: B1): AATree[A, B1] = {
    val cmp = ordering.compare(k, key)

    if (cmp < 0) AANode(key, value, left.insert(k, v), right, level).skew.split
    else if (cmp > 0) AANode(key, value, left, right.insert(k, v), level).skew.split
    else AANode(k, v, left, right, level)
  }

  override def remove(k: A): AATree[A, B] = {
    def decreaseLevel(tree: AATree[A, B]): AATree[A, B] = {
      val shouldBeLevel = Math.min(tree.left.level, tree.right.level) + 1
      tree match {
        case AANode(tk, tv, tl, tr, _) if shouldBeLevel < level =>
          val newRight = tr match {
            case a@AANode(_, _, _, _, lv) if shouldBeLevel < lv => a.copy(level = shouldBeLevel)
            case _ => tr
          }
          AANode(tk, tv, tl, newRight, shouldBeLevel)
        case _ => tree
      }
    }

    def rebalance(tree: AATree[A, B]): AATree[A, B] = {
      val decreasedTree = decreaseLevel(tree).skew

      val beforeSkewing = decreasedTree match {
        case a@AANode(_, _, _, r, _) => a.copy(right = r.skew)
        case x => x
      }
      val afterSkewing = beforeSkewing match {
        case a@AANode(_, _, _, r@AANode(_, _, _, rr, _), _) => a.copy(right = r.copy(right = rr.skew))
        case x => x
      }
      afterSkewing.split match {
        case a@AANode(_, _, _, r, _) => a.copy(right = r.split)
        case x => x
      }
    }

    def del(tree: AATree[A, B]): AATree[A, B] = tree match {
      case AANode(_, _, NilAA(), NilAA(), _) => NilAA[A]()
      case AANode(_, _, l@NilAA(), r, _) => val successor = r.minNode
        rebalance(AANode(successor.key, successor.value, l, r.remove(successor.key), level))
      case AANode(_, _, l, r, _) => val predecessor = l.maxNode
        rebalance(AANode(predecessor.key, predecessor.value, l.remove(predecessor.key), r, level))
      case _ => throw new RuntimeException("Case should be matched by NilAA tree")
    }

    val cmp = ordering.compare(k, key)

    if (cmp < 0) rebalance(AANode(key, value, left.remove(k), right, level))
    else if (cmp > 0) rebalance(AANode(key, value, left, right.remove(k), level))
    else del(this)
  }

  protected def skew: AATree[A, B] = this match {
    case NilAA() => this
    case AANode(_, _, NilAA(), _, _) => this
    case AANode(_, _, AANode(lk, lv, ll, lr, lvl), _, _) if lvl == level =>
      AANode(lk, lv, ll, AANode(key, value, lr, right, level), level)
    case _ => this
  }

  protected def split: AATree[A, B] = this match {
    case NilAA() => this
    case AANode(_, _, _, NilAA(), _) => this
    case AANode(_, _, _, AANode(_, _, _, NilAA(), _), _) => this
    case AANode(_, _, _, AANode(rk, rv, rl, rr, _), lvl) if lvl == rr.level =>
      AANode(rk, rv, AANode(key, value, left, rl, lvl), rr, lvl + 1)
    case _ => this
  }

}

object AATree {
  def apply[A, B]()(implicit ordering: Ordering[A]): BinarySearchTree[A, B] = NilAA[A]()
}

case class NilAA[A]()(implicit val ordering: Ordering[A]) extends NilTree[A] with AATree[A, Nothing] {
  override val level = 0

  override def insert[B](k: A, v: B): AATree[A, B] = AANode[A, B](k, v, this, this)

  override def remove(k: A): AATree[A, Nothing] = this
}

case class AANode[A, B](key: A,
                        value: B,
                        left: AATree[A, B],
                        right: AATree[A, B],
                        override val level: Int = 1)
                       (implicit val ordering: Ordering[A]) extends AATree[A, B]