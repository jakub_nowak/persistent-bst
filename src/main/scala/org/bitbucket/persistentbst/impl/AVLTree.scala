package org.bitbucket.persistentbst.impl

import org.bitbucket.persistentbst._

sealed trait AVLTree[A, +B] extends TreeLike[A, B] {
  def depth: Int

  def balance: Int = right.depth - left.depth

  override def left: AVLTree[A, B]

  override def right: AVLTree[A, B]

  override def insert[B1 >: B](k: A, v: B1): AVLTree[A, B1] = {
    val cmp = ordering.compare(k, key)

    if (cmp < 0) AVLNode(key, value, left.insert(k, v), right).rebalance
    else if (cmp > 0) AVLNode(key, value, left, right.insert(k, v)).rebalance
    else AVLNode(k, v, left, right)
  }

  override def remove(k: A): AVLTree[A, B] = {
    def del(tree: AVLTree[A, B]): AVLTree[A, B] = tree match {
      case AVLNode(_, _, NilAVL(), NilAVL()) => NilAVL[A]()
      case AVLNode(_, _, l, r@AVLNode(_, _, _, _)) => val successor = r.minNode
        AVLNode(successor.key, successor.value, l, r.remove(successor.key).rebalance).rebalance
      case AVLNode(_, _, l@AVLNode(_, _, _, _), r) => val predecessor = l.maxNode
        AVLNode(predecessor.key, predecessor.value, l.remove(predecessor.key).rebalance, r).rebalance
      case _ => throw new RuntimeException("Case should be matched by NilAVL tree")
    }

    val cmp = ordering.compare(k, key)

    if (cmp < 0) AVLNode(key, value, left.remove(k), right).rebalance
    else if (cmp > 0) AVLNode(key, value, left, right.remove(k)).rebalance
    else del(this)
  }

  protected def rebalance: AVLTree[A, B] = {
    if (balance == 2)
      if (right.balance == -1) doubleLeftRotation else leftRotation

    else if (balance == -2)
      if (left.balance == 1) doubleRightRotation else rightRotation

    else this
  }

  private def leftRotation: AVLTree[A, B] = {
    val newLeft = AVLNode(key, value, left, right.left)
    AVLNode(right.key, right.value, newLeft, right.right)
  }

  private def rightRotation: AVLTree[A, B] = {
    val newRight = AVLNode(key, value, left.right, right)
    AVLNode(left.key, left.value, left.left, newRight)
  }

  private def doubleLeftRotation: AVLTree[A, B] = {
    val rightRotated = right.rightRotation
    val newLeft = AVLNode(key, value, left, rightRotated.left)
    AVLNode(rightRotated.key, rightRotated.value, newLeft, rightRotated.right)
  }

  private def doubleRightRotation: AVLTree[A, B] = {
    val leftRotated = left.leftRotation
    val newRight = AVLNode(key, value, leftRotated.right, right)
    AVLNode(leftRotated.key, leftRotated.value, leftRotated.left, newRight)
  }

}

object AVLTree {
  def apply[A, B]()(implicit ordering: Ordering[A]): BinarySearchTree[A, B] = NilAVL[A]()
}

case class NilAVL[A]()(implicit val ordering: Ordering[A]) extends NilTree[A] with AVLTree[A, Nothing] {
  override val depth = 0

  override val balance = 0

  override def insert[B](k: A, v: B): AVLTree[A, B] = AVLNode[A, B](k, v, this, this)

  override def remove(k: A): AVLTree[A, Nothing] = this
}

case class AVLNode[A, B](key: A,
                         value: B,
                         left: AVLTree[A, B],
                         right: AVLTree[A, B])
                        (implicit val ordering: Ordering[A]) extends AVLTree[A, B] {
  override val depth = Math.max(left.depth, right.depth) + 1
}

