package org.bitbucket.persistentbst.impl

import org.bitbucket.persistentbst._

sealed trait Treap[A, +B] extends TreeLike[A, B] {
  def priority: Int

  override def left: Treap[A, B]

  override def right: Treap[A, B]

  override def insert[B1 >: B](k: A, v: B1): Treap[A, B1] = {
    val cmp = ordering.compare(k, key)

    if (cmp < 0) {
      val inserted = TreapNode(key, value, left.insert(k, v), right, priority)
      if (inserted.left.priority > inserted.priority) inserted.rotateRight else inserted
    }
    else if (cmp > 0) {
      val inserted = TreapNode(key, value, left, right.insert(k, v), priority)
      if (inserted.right.priority > inserted.priority) inserted.rotateLeft else inserted
    }
    else TreapNode(k, v, left, right, priority)
  }

  override def remove(k: A): Treap[A, B] = {
    def del(tree: Treap[A, B]): Treap[A, B] = tree match {
      case TreapNode(_, _, NilTreap(), NilTreap(), _) => NilTreap[A]()
      case TreapNode(_, _, TreapNode(_, _, _, _, lp), TreapNode(_, _, _, _, rp), _) if lp > rp =>
        val TreapNode(rk, rv, rl, rr, rp) = tree.rotateRight
        TreapNode(rk, rv, rl, del(rr), rp)
      case TreapNode(_, _, TreapNode(_, _, _, _, lp), NilTreap(), _) =>
        val TreapNode(rk, rv, rl, rr, rp) = tree.rotateRight
        TreapNode(rk, rv, rl, del(rr), rp)
      case _ =>
        val TreapNode(rk, rv, rl, rr, rp) = tree.rotateLeft
        TreapNode(rk, rv, del(rl), rr, rp)
    }

    val cmp = ordering.compare(k, key)

    if (cmp < 0) TreapNode(key, value, left.remove(k), right , priority)
    else if (cmp > 0) TreapNode(key, value, left, right.remove(k), priority)
    else del(this)
  }

  def rotateRight: Treap[A, B] = {
    val newRightNode = TreapNode(key, value, left.right, right, priority)
    TreapNode(left.key, left.value, left.left, newRightNode, left.priority)
  }

  def rotateLeft: Treap[A, B] = {
    val newLeftNode = TreapNode(key, value, left, right.left, priority)
    TreapNode(right.key, right.value, newLeftNode, right.right, right.priority)
  }
}

object Treap {
  def apply[A, B]()(implicit ordering: Ordering[A]): BinarySearchTree[A, B] = NilTreap[A]()
}

case class NilTreap[A]()(implicit val ordering: Ordering[A]) extends NilTree[A] with Treap[A, Nothing] {
  override val priority = -1

  override def insert[B](k: A, v: B): Treap[A, B] = TreapNode[A, B](k, v, this, this)

  override def remove(k: A): Treap[A, Nothing] = this
}

case class TreapNode[A, B](key: A,
                           value: B,
                           left: Treap[A, B],
                           right: Treap[A, B],
                           override val priority: Int = TreapNode.randomPriority())
                          (implicit val ordering: Ordering[A]) extends Treap[A, B]

object TreapNode {
  private[this] val random = new scala.util.Random()

  def randomPriority(): Int = random.nextInt().abs
}
