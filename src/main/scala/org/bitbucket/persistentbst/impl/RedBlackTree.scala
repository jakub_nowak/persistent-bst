package org.bitbucket.persistentbst.impl

import org.bitbucket.persistentbst._

sealed trait RedBlackTree[A, +B] extends TreeLike[A, B] {
  override def left: RedBlackTree[A, B]

  override def right: RedBlackTree[A, B]

  override def insert[B1 >: B](k: A, v: B1): RedBlackTree[A, B1] = {
    val inserted = ins(k, v)
    BlackTree(inserted.key, inserted.value, inserted.left, inserted.right)
  }

  protected def ins[B1 >: B](k: A, v: B1): RedBlackTree[A, B1] = {
    def balance(tree: RBTree[A, B1]): RedBlackTree[A, B1] = tree match {
      case z@BlackTree(zk, zv, y@RedTree(yk, yv, x@RedTree(xk, xv, a, b), c), d) =>
        RedTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case z@BlackTree(zk, zv, x@RedTree(xk, xv, a, y@RedTree(yk, yv, b, c)), d) =>
        RedTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case x@BlackTree(xk, xv, a, y@RedTree(yk, yv, b, z@RedTree(zk, zv, c, d))) =>
        RedTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case x@BlackTree(xk, xv, a, z@RedTree(zk, zv, y@RedTree(yk, yv, b, c), d)) =>
        RedTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case x => x
    }

    val cmp = ordering.compare(k, key)

    if (cmp < 0) this match {
      case r: RedTree[A, B] => balance(r.copy(left = left.ins(k, v)))
      case b: BlackTree[A, B] => balance(b.copy(left = left.ins(k, v)))
      case _ => throw new RuntimeException("Case should be matched by NilRedBlack tree")
    }
    else if (cmp > 0) this match {
      case r: RedTree[A, B] => balance(r.copy(right = right.ins(k, v)))
      case b: BlackTree[A, B] => balance(b.copy(right = right.ins(k, v)))
      case _ => throw new RuntimeException("Case should be matched by NilRedBlack tree")
    }
    else {
      this match {
        case r: RedTree[A, B] => r.copy(value = v)
        case b: BlackTree[A, B] => b.copy(value = v)
        case _ => throw new RuntimeException("Case should be matched by NilRedBlack tree")
      }
    }
  }

  override def remove(k: A): RedBlackTree[A, B] = {
    val removed = removeImpl(k)

    if (removed.isInstanceOf[DoubleBlackNil[A, B]]) NilRedBlack[A]()
    else BlackTree(removed.key, removed.value, removed.left, removed.right)
  }

  protected def removeImpl(k: A): RedBlackTree[A, B] = {
    def balance(tree: RBTree[A, B]): RedBlackTree[A, B] =  tree match {
      //classic
      case z@BlackTree(zk, zv, y@RedTree(yk, yv, x@RedTree(xk, xv, a, b), c), d) =>
        RedTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case z@BlackTree(zk, zv, x@RedTree(xk, xv, a, y@RedTree(yk, yv, b, c)), d) =>
        RedTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case x@BlackTree(xk, xv, a, y@RedTree(yk, yv, b, z@RedTree(zk, zv, c, d))) =>
        RedTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case x@BlackTree(xk, xv, a, z@RedTree(zk, zv, y@RedTree(yk, yv, b, c), d)) =>
        RedTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))

      //double black
      case z@DoubleBlackTree(zk, zv, y@RedTree(yk, yv, x@RedTree(xk, xv, a, b), c), d) =>
        BlackTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case z@DoubleBlackTree(zk, zv, x@RedTree(xk, xv, a, y@RedTree(yk, yv, b, c)), d) =>
        BlackTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case x@DoubleBlackTree(xk, xv, a, y@RedTree(yk, yv, b, z@RedTree(zk, zv, c, d))) =>
        BlackTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))
      case x@DoubleBlackTree(xk, xv, a, z@RedTree(zk, zv, y@RedTree(yk, yv, b, c), d)) =>
        BlackTree(yk, yv, BlackTree(xk, xv, a, b), BlackTree(zk, zv, c, d))

      //negative black
      case z@DoubleBlackTree(zk, zv, x@NegativeBlackTree(xk, xv, w@BlackTree(wk, wv, a, b), y@BlackTree(yk, yv, c, d)), e) =>
        BlackTree(yk, yv, balance(BlackTree(xk, xv, RedTree(wk, wv, a, b), c)), BlackTree(zk, zv, d, e))
      case z@DoubleBlackTree(zk, zv, a, x@NegativeBlackTree(xk, xv, w@BlackTree(wk, wv, b, c), y@BlackTree(yk, yv, d, e))) =>
        BlackTree(wk, wv, BlackTree(zk, zv, a, b), balance(BlackTree(xk, xv, c, RedTree(yk, yv, d, e))))

      case x => x
    }

    def bubble(tree: RBTree[A, B]): RedBlackTree[A, B] = tree match {
      case y@BlackTree(yk, yv, x@BlackTree(xk, xv, xl, xr), z@DoubleBlackTree(zk, zv, zl, zr)) =>
        balance(new DoubleBlackTreeNode(yk, yv, RedTree(xk, xv, xl, xr), z.redden))
      case y@RedTree(yk, yv, x@BlackTree(xk, xv, xl, xr), z@DoubleBlackTree(zk, zv, zl, zr)) =>
        balance(BlackTree(yk, yv, RedTree(xk, xv, xl, xr), z.redden))
      case y@BlackTree(yk, yv, x@RedTree(xk, xv, xl, xr), z@DoubleBlackTree(zk, zv, zl, zr)) =>
        balance(new DoubleBlackTreeNode(yk, yv, NegativeBlackTree(xk, xv, xl, xr), z.redden))
      case y@BlackTree(yk, yv, x@DoubleBlackTree(xk, xv, xl, xr), z@BlackTree(zk, zv, zl, zr)) =>
        balance(new DoubleBlackTreeNode(yk, yv, x.redden, RedTree(zk, zv, zl, zr)))
      case y@RedTree(yk, yv, x@DoubleBlackTree(xk, xv, xl, xr), z@BlackTree(zk, zv, zl, zr)) =>
        balance(BlackTree(yk, yv, x.redden, RedTree(zk, zv, zl, zr)))
      case y@BlackTree(yk, yv, x@DoubleBlackTree(xk, xv, xl, xr), z@RedTree(zk, zv, zl, zr)) =>
        balance(new DoubleBlackTreeNode(yk, yv, x.redden, NegativeBlackTree(zk, zv, zl, zr)))
      case x => x
    }

    def del(tree: RedBlackTree[A, B]): RedBlackTree[A, B] = tree match {
      case RedTree(_, _ , NilRedBlack(), NilRedBlack()) => NilRedBlack[A]()
      case BlackTree(xk, xv , NilRedBlack(), NilRedBlack()) => new DoubleBlackNil[A, B](xk, xv, NilRedBlack(), NilRedBlack())

      case BlackTree(_, _, NilRedBlack(), y@RedTree(yk, yv, yl, yr)) => BlackTree(yk, yv, yl, yr)
      case BlackTree(_, _, y@RedTree(yk, yv, yl, yr), NilRedBlack()) => BlackTree(yk, yv, yl, yr)

      case BlackTree(_, _, l, r) => val predecessor = l.maxNode
        bubble(BlackTree(predecessor.key, predecessor.value, l.removeImpl(predecessor.key), r))
      case RedTree(_, _, l, r) => val predecessor = l.maxNode
        bubble(RedTree(predecessor.key, predecessor.value, l.removeImpl(predecessor.key), r))

      case _ => throw new RuntimeException()
    }

    val cmp = ordering.compare(k, key)

    if (cmp < 0) this match {
      case r: RedTree[A, B] => bubble(r.copy(left = left.removeImpl(k)))
      case b: BlackTree[A, B] => bubble(b.copy(left = left.removeImpl(k)))
      case _ => throw new RuntimeException("Case should be matched by NilRedBlack tree")
    }
    else if (cmp > 0) this match {
      case r: RedTree[A, B] => bubble(r.copy(right = right.removeImpl(k)))
      case b: BlackTree[A, B] => bubble(b.copy(right = right.removeImpl(k)))
      case _ => throw new RuntimeException("Case should be matched by NilRedBlack tree")
    }
    else del(this)
  }

}

case class NilRedBlack[A]()(implicit val ordering: Ordering[A]) extends NilTree[A] with RedBlackTree[A, Nothing] {
  override def ins[B](k: A, v: B): RedBlackTree[A, B] = RedTree(k, v, this, this)

  override def remove(k: A): RedBlackTree[A, Nothing] = this

  override protected def removeImpl(k: A): RedBlackTree[A, Nothing] = this
}

object RedBlackTree {
  def apply[A, B]()(implicit ordering: Ordering[A]): BinarySearchTree[A, B] = NilRedBlack[A]()
}

sealed abstract class RBTree[A, +B](key: A,
                           value: B,
                           left: RedBlackTree[A, B],
                           right: RedBlackTree[A, B]) extends RedBlackTree[A, B]

case class RedTree[A, +B](key: A,
                          value: B,
                          left: RedBlackTree[A, B],
                          right: RedBlackTree[A, B])
                          (implicit val ordering: Ordering[A]) extends RBTree[A, B](key, value, left, right)

case class BlackTree[A, +B](key: A,
                            value: B,
                            left: RedBlackTree[A, B],
                            right: RedBlackTree[A, B])
                         (implicit val ordering: Ordering[A]) extends RBTree[A, B](key, value, left, right)

/**
  * These extra nodes exists only for deletion purpose
  */

abstract case class DoubleBlackTree[A, +B](key: A,
                                  value: B,
                                  left: RedBlackTree[A, B],
                                  right: RedBlackTree[A, B])
                                 (implicit val ordering: Ordering[A]) extends RBTree[A, B](key, value, left, right) {
  def redden: RedBlackTree[A, B]

}

class DoubleBlackTreeNode[A, +B](key: A,
                                 value: B,
                                 left: RedBlackTree[A, B],
                                 right: RedBlackTree[A, B])
                                (implicit override val ordering: Ordering[A]) extends DoubleBlackTree[A, B](key, value, left, right) {
  def redden = BlackTree(key, value, left, right)
}

class DoubleBlackNil[A, +B](key: A,
                            value: B,
                            left: RedBlackTree[A, B],
                            right: RedBlackTree[A, B])
                           (implicit override val ordering: Ordering[A])extends DoubleBlackTree[A, B](key, value, left, right) {
  def redden = NilRedBlack[A]()
}


case class NegativeBlackTree[A, +B](key: A,
                                    value: B,
                                    left: RedBlackTree[A, B],
                                    right: RedBlackTree[A, B])
                                   (implicit val ordering: Ordering[A]) extends RBTree[A, B](key, value, left, right)