package org.bitbucket.persistentbst.impl

import org.bitbucket.persistentbst._

sealed trait UnbalancedTree[A, +B] extends TreeLike[A, B] {
  override def left: UnbalancedTree[A, B]

  override def right: UnbalancedTree[A, B]

  override def insert[B1 >: B](k: A, v: B1): UnbalancedTree[A, B1] = {
    val cmp = ordering.compare(k, key)

    if (cmp < 0) UnbalancedNode(key, value, left.insert(k, v), right)
    else if (cmp > 0) UnbalancedNode(key, value, left, right.insert(k, v))
    else UnbalancedNode(k, v, left, right)
  }

  override def remove(k: A): UnbalancedTree[A, B] = {
    def del(tree: UnbalancedTree[A, B]): UnbalancedTree[A, B] = tree match {
      case UnbalancedNode(_, _, NilUnbalanced(), NilUnbalanced()) => NilUnbalanced[A]()
      case UnbalancedNode(_, _, l@UnbalancedNode(_, _, _, _), NilUnbalanced()) => l
      case UnbalancedNode(_, _, NilUnbalanced(), r@UnbalancedNode(_, _, _, _)) => r
      case UnbalancedNode(_, _, l@UnbalancedNode(_, _, _, _), r@UnbalancedNode(_, _, _, _)) => val successor = r.minNode
        UnbalancedNode(successor.key, successor.value, l, r.remove(successor.key))
      case _ => throw new RuntimeException("Case should be matched by NilUnbalanced tree")
    }

    val cmp = ordering.compare(k, key)

    if (cmp < 0) UnbalancedNode(key, value, left.remove(k), right)
    else if (cmp > 0) UnbalancedNode(key, value, left, right.remove(k))
    else del(this)
  }

}

object UnbalancedTree {
  def apply[A, B]()(implicit ordering: Ordering[A]): BinarySearchTree[A, B] = NilUnbalanced[A]()
}

case class NilUnbalanced[A]()(implicit val ordering: Ordering[A]) extends NilTree[A] with UnbalancedTree[A, Nothing] {
  override def insert[B](k: A, v: B): UnbalancedTree[A, B] = UnbalancedNode[A, B](k, v, this, this)

  override def remove(k: A): UnbalancedTree[A, Nothing] = this
}

case class UnbalancedNode[A, +B](key: A,
                                 value: B,
                                 left: UnbalancedTree[A, B],
                                 right: UnbalancedTree[A, B])
                                (implicit val ordering: Ordering[A]) extends UnbalancedTree[A, B]