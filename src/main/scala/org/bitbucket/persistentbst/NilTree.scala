package org.bitbucket.persistentbst

abstract class NilTree[A] extends TreeLike[A, Nothing] {
  override def key: A = null.asInstanceOf[A]

  override def value: Nothing = fail

  override lazy val size = 0

  override def left: Nothing = fail

  override def right: Nothing = fail
  
  override def toList: List[(A, Nothing)] = Nil

  override def contains(k: A): Boolean = false

  override def get(k: A): Option[Nothing] = None

  private def fail: Nothing = throw new NoSuchElementException

}
