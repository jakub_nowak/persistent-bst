package org.bitbucket.persistentbst

trait BinarySearchTree[A, +B] {
  def insert[B1 >: B](k: A, v: B1): BinarySearchTree[A, B1]

  def insertAll[B1 >: B](elements: List[(A, B1)]): BinarySearchTree[A, B1]

  def remove(k: A): BinarySearchTree[A, B]

  def contains(k: A): Boolean

  def get(k: A): Option[B]

  def toList: List[(A, B)]

  def size: Int
}