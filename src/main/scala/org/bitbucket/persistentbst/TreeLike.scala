package org.bitbucket.persistentbst

abstract class TreeLike[A, +B] extends BinarySearchTree[A, B] {
  def key: A

  def value: B

  def left: TreeLike[A, B]

  def right: TreeLike[A, B]

  override lazy val size: Int = left.size + right.size + 1

  def isEmpty: Boolean = size == 0

  def nonEmpty = !isEmpty

  implicit def ordering: Ordering[A]

  override def contains(k: A): Boolean = {
    val cmp = ordering.compare(k, key)

    if (cmp < 0) left.contains(k)
    else if (cmp > 0) right.contains(k)
    else true
  }

  override def get(k: A): Option[B] = {
    val cmp = ordering.compare(k, key)

    if (cmp < 0) left.get(k)
    else if (cmp > 0) right.get(k)
    else Some(value)
  }

  def toList: List[(A, B)] =
    left.toList ::: List((key, value)) ::: right.toList

  def insert[B1 >: B](k: A, v: B1): TreeLike[A, B1]

  def remove(k: A): TreeLike[A, B]

  def insertAll[B1 >: B](elements: List[(A, B1)]): TreeLike[A, B1] =
    elements.foldLeft[TreeLike[A, B1]](this){ case (tree, (k, v)) => tree.insert(k, v) }

  def minNode: TreeLike[A, B] = {
    if (left.isEmpty) this
    else left.minNode
  }

  def maxNode: TreeLike[A, B] = {
    if (right.isEmpty) this
    else right.maxNode
  }

}