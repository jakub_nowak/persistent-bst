package org.bitbucket.persistentbst

import org.bitbucket.persistentbst.impl._
import org.scalatest._
import org.scalatest.prop.{GeneratorDrivenPropertyChecks, PropertyChecks}

trait BstImplementationTest extends WordSpecLike with ShouldMatchers with PropertyChecks with GeneratorDrivenPropertyChecks {
  val bst: TreeLike[Int, Int]

  "A proper implementation" should {

    "have tree invariant after insert operations" in {
      forAll { (scores: List[Int]) =>
        val elems = scores.zipWithIndex
        var resultBst = bst

        elems.foreach { case (s, e) =>
          resultBst = resultBst insert(s, e)

          treeInvariant(resultBst) shouldBe true
          specificInvariant(resultBst) shouldBe true
        }
      }
    }

    "have tree invariant after remove operations" in {
      forAll { (scores: List[Int]) =>
        val elems = scores.zipWithIndex
        var resultBst = bst.insertAll(elems)

        scala.util.Random.shuffle(elems).foreach { case (s, e) =>
          resultBst = resultBst remove s

          treeInvariant(resultBst) shouldBe true
          specificInvariant(resultBst) shouldBe true
        }
      }
    }

    "have tree invariant after mixed operations" in {
      def booleanStream: Stream[Boolean] = scala.util.Random.nextBoolean() #:: booleanStream

      forAll { (scores: List[Int]) =>
        val elems = scores.zipWithIndex
        val toRemove = elems zip booleanStream.take(elems.size) filter { case ((s, e), removed) => removed} map {_._1}

        var resultBst = bst.insertAll(elems)
        scala.util.Random.shuffle(toRemove) foreach { case (s, e) =>
          resultBst = resultBst remove s

          treeInvariant(resultBst) shouldBe true
          specificInvariant(resultBst) shouldBe true
        }
        scala.util.Random.shuffle(toRemove) foreach { case (s, e) =>
          resultBst = resultBst insert (s, e)

          treeInvariant(resultBst) shouldBe true
          specificInvariant(resultBst) shouldBe true
        }
      }
    }
  }

  def specificInvariant(tree: BinarySearchTree[Int, _]): Boolean

  def treeInvariant(tree: TreeLike[Int, _]): Boolean = {
    if (tree.isEmpty) true
    else if (tree.left.nonEmpty && tree.right.nonEmpty)
      tree.ordering.compare(tree.left.key, tree.key) < 0 &&
        tree.ordering.compare(tree.right.key, tree.key) > 0 &&
        treeInvariant(tree.left) &&  treeInvariant(tree.right)
    else if (tree.left.nonEmpty)
      tree.ordering.compare(tree.left.key, tree.key) < 0 && treeInvariant(tree.left)
    else if (tree.left.nonEmpty)
      tree.ordering.compare(tree.right.key, tree.key) > 0 && treeInvariant(tree.right)
    else true
  }
}

class UnbalancedImplementationTest extends BstImplementationTest {
  override val bst = UnbalancedTree[Int, Int]().asInstanceOf[TreeLike[Int, Int]]

  override def specificInvariant(tree: BinarySearchTree[Int, _]): Boolean = true

}

class TreapImplementationTest extends BstImplementationTest {
  override val bst = Treap[Int, Int]().asInstanceOf[TreeLike[Int, Int]]

  override def specificInvariant(tree: BinarySearchTree[Int, _]): Boolean = {
    def inner(tree: Treap[Int, _]): Boolean = tree match {
      case NilTreap() => true
      case TreapNode(_, _, l, r, p) => l.priority <= p && r.priority <= p && inner(l) && inner(r)
    }

    inner(tree.asInstanceOf[Treap[Int, _]])
  }
}

class AAImplementationTest extends BstImplementationTest {
  override val bst = AATree[Int, Int]().asInstanceOf[TreeLike[Int, Int]]

  override def specificInvariant(tree: BinarySearchTree[Int, _]): Boolean = {
    def inner(tree: AATree[Int, _]): Boolean = tree match {
      case NilAA() => tree.level == 0
      case AANode(_, _, NilAA(), NilAA(), lv) => lv == 1
      case AANode(_, _, l, r, lv) =>
        l.level + 1 == lv &&
        (0 to 1).exists(_ == lv - r.level) &&
        inner(tree.left) && inner(tree.right) &&
        (if (r.nonEmpty) r.right.level < lv && r.left.level < lv else true) &&
        (if (lv > 1) l.nonEmpty && r.nonEmpty else true)
    }

    inner(tree.asInstanceOf[AATree[Int, _]])
  }
}

class AVLImplementationTest extends BstImplementationTest {
  override val bst = AVLTree[Int, Int]().asInstanceOf[TreeLike[Int, Int]]

  override def specificInvariant(tree: BinarySearchTree[Int, _]): Boolean = {
    def inner(tree: AVLTree[Int, _]): Boolean = tree match {
      case a@NilAVL() => a.depth == 0
      case a@AVLNode(_, _ , l, r) => a.depth == 1 + math.max(l.depth, r.depth)
    }

    inner(tree.asInstanceOf[AVLTree[Int, _]])
  }
}

class RedBlackImplementationTest extends BstImplementationTest {
  override val bst = RedBlackTree[Int, Int]().asInstanceOf[TreeLike[Int, Int]]

  override def specificInvariant(tree: BinarySearchTree[Int, _]): Boolean = {
    def redInvariant(tree: RedBlackTree[Int, _]): Boolean = tree match {
      case _: NilRedBlack[Int] => true
      case RedTree(_, _, l@BlackTree(_, _, _, _), r@BlackTree(_, _, _, _)) => redInvariant(l) && redInvariant(r)
      case RedTree(_, _, l@BlackTree(_, _, _, _), r: NilRedBlack[Int]) => redInvariant(l)
      case RedTree(_, _, l: NilRedBlack[Int], r@BlackTree(_, _, _, _)) => redInvariant(r)
      case RedTree(_, _, l: NilRedBlack[Int], r: NilRedBlack[Int]) => true
      case BlackTree(_, _, l, r) => redInvariant(l) && redInvariant(r)
      case _ => false
    }

    def blackInvariant(tree: RedBlackTree[Int, _]): (Boolean, Int) = tree match {
      case NilRedBlack() => (true, 0)
      case t: RedBlackTree[Int, _] =>
        val (iLeft, dLeft) = blackInvariant(t.left)
        val (iRight, dRight) = blackInvariant(t.right)
        (dLeft == dRight && iLeft && iRight, dLeft)
    }

    val rb = tree.asInstanceOf[RedBlackTree[Int, _]]
    (rb.isEmpty || rb.isInstanceOf[BlackTree[Int, _]]) && redInvariant(rb) && blackInvariant(rb)._1
  }
}