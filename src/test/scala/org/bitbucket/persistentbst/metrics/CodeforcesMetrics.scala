package org.bitbucket.persistentbst.metrics

import org.bitbucket.persistentbst._
import org.bitbucket.persistentbst.impl._
import org.bitbucket.persistentbst.metrics.utils.CodeforcesScore
import org.scalameter.api._

trait CodeforcesMetrics extends Bench.ForkedTime {
  def tree: BinarySearchTree[Int, Set[String]]
  def treeName: String

  performance of treeName in {

    measure method "construction" in {
      var bst = tree
      using(GenUtils.codeforcesScores) in { elems =>
        elems foreach { case CodeforcesScore(user, time, oldScore, newScore) =>
          bst.get(oldScore) foreach { els =>
            val withoutUser = els - user
            bst = bst.remove(oldScore)
            if (withoutUser.nonEmpty) bst = bst.insert(oldScore, withoutUser)
          }

           bst = bst.get(newScore)
             .map( els => bst.remove(newScore).insert(newScore, els + user) )
             .getOrElse( bst.insert(newScore, Set(user)) )
        }
      }
    }

    measure method "contains" in {
      using(GenUtils.codeforcesTree(tree)) in { case (bst, elems) =>
        elems foreach { case CodeforcesScore(user, time, oldScore, newScore) =>
          bst get newScore exists (_.contains(user))
        }
      }
    }

  }
}

object TreapCodeforcesBenchmark extends CodeforcesMetrics {
  override def tree = Treap[Int, Set[String]]()

  override def treeName: String = "Treap"
}

object AATreeCodeforcesBenchmark extends CodeforcesMetrics {
  override def tree = AATree[Int, Set[String]]()

  override def treeName: String = "AA tree"
}

object AVLTreeCodeforcesBenchmark extends CodeforcesMetrics {
  override def tree = AVLTree[Int, Set[String]]()

  override def treeName: String = "AVL tree"
}

object UnbalancedTreeCodeforcesBenchmark extends CodeforcesMetrics {
  override def tree = UnbalancedTree[Int, Set[String]]()

  override def treeName: String = "Unbalanced tree"
}

object RedBlackTreeCodeforcesBenchmark extends CodeforcesMetrics {
  override def tree = RedBlackTree[Int, Set[String]]()

  override def treeName: String = "Red-Black tree"
}