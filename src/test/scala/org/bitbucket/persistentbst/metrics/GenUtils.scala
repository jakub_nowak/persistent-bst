package org.bitbucket.persistentbst.metrics

import org.bitbucket.persistentbst.BinarySearchTree
import org.bitbucket.persistentbst.metrics.utils.CodeforcesScore
import org.scalameter.api._
import org.scalameter.picklers.Implicits.intPickler

import scala.io.Source
import scala.util.Random

object GenUtils {
  val sizes: Gen[Int] = Gen.enumeration("Number of queries")(1000, 10000, 50000, 100000, 500000, 1000000)

  val lines = Source.fromFile("./src/test/resources/codeforces_ranking").getLines()

  val codeforces = loadCodeforcesScores().toList.sortBy(_.time)

  val scores: Gen[List[(Int, Int)]] = {
    val rand = new Random()
    rand.setSeed(100)
    for {
      size <- sizes
    } yield Seq.fill(size)(rand.nextInt()).toList.zipWithIndex
  }

  def trees(bst: BinarySearchTree[Int, Int]): Gen[(BinarySearchTree[Int, Int], List[(Int, Int)])] = {
    val rand = new Random()
    rand.setSeed(100)
    for {
      size <- sizes
    } yield {
      val elems = Seq.fill(size)(rand.nextInt()).toList.zipWithIndex
      (bst insertAll elems, elems)
    }
  }

  val codeforcesScores: Gen[List[CodeforcesScore]] = {
    for {
      size <- sizes
    } yield codeforces.take(size)
  }

  def codeforcesTree(bst: BinarySearchTree[Int, Set[String]]): Gen[(BinarySearchTree[Int, Set[String]], List[CodeforcesScore])] = {
    for {
      size <- sizes
      scores = codeforces.take(size)
    } yield {
      var resultBst = bst
      scores.foreach { case CodeforcesScore(user, time, oldScore, newScore) =>
        resultBst.get(oldScore) foreach { els =>
          val withoutUser = els - user
          resultBst = resultBst.remove(oldScore)
          if (withoutUser.nonEmpty) resultBst = resultBst.insert(oldScore, withoutUser)
        }

        resultBst = resultBst.get(newScore)
          .map( els => resultBst.remove(newScore).insert(newScore, els + user) )
          .getOrElse( resultBst.insert(newScore, Set(user)) )
      }
      (resultBst, scores)
    }
  }

  private def loadCodeforcesScores(): Iterator[CodeforcesScore]  = {
    val scores = for (line <- lines;
         pattern = """^([.A-Za-z0-9_-]+) ([0-9]+) (-?[0-9]+) (-?[0-9]+)""".r;
         pattern(user, time, oldScore, newScore) = line)
      yield CodeforcesScore(user, time.toInt, oldScore.toInt, newScore.toInt)
    scores
  }

}