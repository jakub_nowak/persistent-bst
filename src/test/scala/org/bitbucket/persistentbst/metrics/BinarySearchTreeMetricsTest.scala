package org.bitbucket.persistentbst.metrics

import org.bitbucket.persistentbst._
import org.bitbucket.persistentbst.impl._
import org.scalameter.api._

trait BinarySearchTreeMetricsTest extends Bench.ForkedTime {
  def tree: BinarySearchTree[Int, Int]
  def treeName: String

  performance of treeName in {

    measure method "insert" in {
      var bst = tree
      using(GenUtils.scores) in { elems =>
        elems foreach { case (s, e) =>
          bst = bst insert (s, e)
        }
      }
    }

    measure method "remove" in {
      using(GenUtils.trees(tree)) in { case (bst, elems) =>
        elems foreach { case (s, e) =>
          bst remove s
        }
      }
    }

    measure method "contains" in {
      using(GenUtils.trees(tree)) in { case (bst, elems) =>
        elems foreach { case (s, e) =>
          bst contains s
        }
      }
    }

  }
}

object TreapBenchmark extends BinarySearchTreeMetricsTest {
  override def tree = Treap[Int, Int]()

  override def treeName: String = "Treap"
}

object AATreeBenchmark extends BinarySearchTreeMetricsTest {
  override def tree = AATree[Int, Int]()

  override def treeName: String = "AA tree"
}

object AVLTreeBenchmark extends BinarySearchTreeMetricsTest {
  override def tree = AVLTree[Int, Int]()

  override def treeName: String = "AVL tree"
}

object UnbalancedTreeBenchmark extends BinarySearchTreeMetricsTest {
  override def tree = UnbalancedTree[Int, Int]()

  override def treeName: String = "Unbalanced tree"
}

object RedBlackTreeBenchmark extends BinarySearchTreeMetricsTest {
  override def tree = RedBlackTree[Int, Int]()

  override def treeName: String = "Red-Black tree"
}