package org.bitbucket.persistentbst.metrics.utils

case class CodeforcesScore(user: String, time: Int, oldScore: Int, newScore: Int)
