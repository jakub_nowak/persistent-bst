package org.bitbucket.persistentbst

import org.bitbucket.persistentbst.impl._
import org.scalatest.prop.{GeneratorDrivenPropertyChecks, PropertyChecks}
import org.scalatest.{ShouldMatchers, WordSpecLike}

import scala.util.Random

trait BinarySearchTreeTest extends WordSpecLike with ShouldMatchers with PropertyChecks with GeneratorDrivenPropertyChecks {
  val bst: TreeLike[Int, Int]

  "A binary tree" when {

    "non empty" should {

      "have proper size" in {
        forAll { (scores: List[Int]) =>
          val elems = scores.zipWithIndex
          bst.insertAll(elems).size shouldBe scores.distinct.size
        }
      }

      "not be empty" in {
        forAll { (scores: List[Int]) =>
          whenever(scores.nonEmpty) {
            val elems = scores.zipWithIndex
            val resultBst = bst.insertAll(elems)
            resultBst.isEmpty shouldBe false
            resultBst.nonEmpty shouldBe true
          }
        }
      }

      "become empty after removing all added elements" in {
        forAll { (scores: List[Int]) =>
          val elems = scores.zipWithIndex
          var result = bst.insertAll(elems)
          elems.foreach{case (s, e) => result = result.remove(s)}
          true shouldBe true
          result.isEmpty shouldBe true
        }
      }
    }

    "insert different elements" should {

      "contain inserted elements" in {
        forAll { (scores: List[Int]) =>
          val elems = scores.zipWithIndex
          val resultBst = bst.insertAll(elems)

          elems foreach { case (s, e) => resultBst.contains(s) shouldBe true }
        }
      }
    }

    "insert same elements" should {

      "not change itself" in {
        forAll { (scores: List[Int]) =>
          val elems = scores.zipWithIndex
          val resultBst = bst.insertAll(elems)
          resultBst shouldEqual resultBst.insertAll(elems)
        }
      }
    }

    "remove elements" should {

      "not contain removed elements" in {
        forAll { (scores: List[Int]) =>
          val elems = scores.zipWithIndex
          var tree = bst.insertAll(elems)

          val shuffledElems = Random.shuffle(elems)
          shuffledElems foreach { case (s, e) =>
            tree = tree remove s

            tree.contains(s) shouldBe false
          }

          tree.isEmpty shouldBe true
        }
      }

      "allow to insert deleted elements" in {
        def booleanStream: Stream[Boolean] = scala.util.Random.nextBoolean() #:: booleanStream

        forAll { (scores: List[Int]) =>
          val elems = scores.distinct.zipWithIndex
          val toRemove = elems zip booleanStream.take(elems.size) filter { case ((s, e), removed) => removed } map { _._1 }

          var result = bst.insertAll(elems)
          toRemove foreach { case (s, e) =>
            result = result remove s
          }
          toRemove foreach { case (s, e) =>
            result = result insert(s, e)
          }

          result.toList shouldEqual (elems diff toRemove union toRemove).sortBy { case (s, e) => s }
        }
      }

        "leave other elements sorted" in {
          def booleanStream: Stream[Boolean] = scala.util.Random.nextBoolean() #:: booleanStream

          forAll { (scores: List[Int]) =>
            val elems = scores.distinct.zipWithIndex
            val toRemove = elems zip booleanStream.take(elems.size) filter { case ((s, e), removed) => removed } map { _._1 }

            var result = bst.insertAll(elems)
            toRemove foreach { case (s, e) =>
              result = result remove s
            }

            result.toList shouldEqual (elems diff toRemove).sortBy { case (s, e) => s }
          }
        }
      }

    "list elements" should {

      "sort them by score" in {
        forAll { (scores: List[Int]) =>
          val elems = scores.distinct.zipWithIndex
          bst.insertAll(elems).toList.map { case (s, e) => e } shouldEqual elems.sortBy(_._1).map { _._2 }
        }
      }
    }
  }
}

class UnbalancedBinaryTreeTest extends BinarySearchTreeTest {
  override val bst = UnbalancedTree[Int, Int]().asInstanceOf[TreeLike[Int, Int]]
}

class TreapTest extends BinarySearchTreeTest {
  override val bst = Treap[Int, Int]().asInstanceOf[TreeLike[Int, Int]]
}

class AATreeTest extends BinarySearchTreeTest {
  override val bst = AATree[Int, Int]().asInstanceOf[TreeLike[Int, Int]]
}

class AVLTreeTest extends BinarySearchTreeTest {
  override val bst = AVLTree[Int, Int]().asInstanceOf[TreeLike[Int, Int]]
}

class RedBlackTreeTest extends BinarySearchTreeTest {
  override val bst = RedBlackTree[Int, Int]().asInstanceOf[TreeLike[Int, Int]]
}